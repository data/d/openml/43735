# OpenML dataset: NYC-Hourly-Temperature

https://www.openml.org/d/43735

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Hourly weather data for New York City. Extracted from online web sources. The following data set is cleaned for the purpose for NYC Taxi ETA calculation.
Content
We have features such as Date, Time, temperature (F), Dew Point (F), Humidity, Wind Speed (MPH), Condition.
Acknowledgements
The cleaned version is user owned. Used in past research for weather data analysis in Boston. Performed the similar calculation to extract the dataset.
Inspiration
The hourly dataset is cleaned with no missing values. Along with temperature the dataset also consists of features like Humidity and Condition such as snow, rain etc.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43735) of an [OpenML dataset](https://www.openml.org/d/43735). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43735/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43735/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43735/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

